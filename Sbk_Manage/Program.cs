﻿/*   
'                   _ooOoo_
'                  o8888888o
'                  88" . "88
'                  (| -_- |)
'                  O\  =  /O
'               ____/`---'\____
'             .'  \\|     |//  `.
'            /  \\|||  :  |||//  \
'           /  _||||| -:- |||||-  \
'           |   | \\\  -  /// |   |
'           | \_|  ''\---/''  |   |
'           \  .-\__  `-`  ___/-. /
'         ___`. .'  /--.--\  `. . __
'      ."" '<  `.___\_<|>_/___.'  >'"".
'     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
'     \  \ `-.   \_ __\ /__ _/   .-` /  /
'======`-.____`-.___\_____/___.-`____.-'======
'                   `=---='
'^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
'         佛祖保佑       永无BUG
'==============================================================================
'文件
'名称: 麦禾合同管理系统
'功能: 合同管理完整解决方案
'作者: peer
'日期: 2017.05.18
'修改:
'日期:
'备注:
'==============================================================================
*/
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using PeerPublicLib;
namespace Sbk_Manage
{

    static class Program
    {
        //数据库操作对象
        public static SqliteVbNetHelper SqliteDB;
        //
        public static FrmMain FrmMainIns;
        //操作人员姓名
        public static string User_Name = "admin";
        //操作人员密码
        public static string User_Pwd = "admin";
        //应用程序执行路径
        public static string APPdirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);

        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            //本地库 查询当前本地库是否存在
            Program.SqliteDB = new SqliteVbNetHelper(Application.StartupPath, "DBCommon");
            //
            SimpleInstanceApplication simpleInstance = new SimpleInstanceApplication();
            simpleInstance.Run(args);
        }
    }
}
